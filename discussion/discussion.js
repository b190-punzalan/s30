db.fruits.insertMany([
	{
	name: "Apple",
    color: "red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: [ "Philippines", "US" ]
    },
    {
	name: "Banana",
    color: "yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: [ "Philippines", "Ecuador" ]
    },
    {
	name: "Kiwi",
    color: "green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: [ "US", "China" ]
    },
    {
	name: "Mango",
    color: "green",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: [ "Philippines", "India" ]
    }
]);



/*
// create another collection inside b-190-course-booking called "fruits"
/*
    insert the following fruit objects along with their properties
        1st object:
            name: apple
            color: red
            stock: 20
            price: 40
            supplier_id: 1
            onSale: true
            origin: [ Philippines and US ]

        2nd object:
            name: banana
            color: yellow
            stock: 15
            price: 20
            supplier_id: 2
            onSale: true
            origin: [ Philippines and Ecuador ]

        3rd object:
            name: kiwi
            color: green
            stock: 25
            price: 50
            supplier_id: 1
            onSale: true
            origin: [ US and China ]

        4th object:
            name:mango
            color: green
            stock: 10
            price: 120
            supplier_id: 2
            onSale: false
            origin: [ Philippines and India ]
*/

// Section - MongoDB Aggregation
/*
- used to generate manipulated data and perform operations to create filtered results that help in analysis of data. 
-Compared to CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with info to make necessary development decisions without haing to create a frontend application

*/

// Section - Using Aggregate Method
/*
$match

- used to pass the document that meet the specified condition/s to the next pipeline/aggregaton process 
-pipelines/aggregation process is the series of aggregation methods should the dev/client want to use two or more aggregation methods in one statement. MongoDB will treat this as stages wherein it will not proceed to the next pipeline unless it is done with the first
	Syntax:
		{$match: {field: value}}


*/
db.fruits.aggregate([
		{$match: {onSale: true}}
]);

// $group
/*
- used to group elements together and fielld-value pairs using the data from the grouped elements
Syntax:
	{$group: {_id: "$value", fieldResult: {$valueResult"}}}
*/
db.fruits.aggregate([
		{$group: {_id:"$supplier_id", total: {$sum: "$stock"}}}
]);
// using both $match and $group along with aggregation will find for the products...
// Syntax:
/*db.collectionName.aggregate([
	{$match: {field: value}},
	{$group: {_id: "$value", fieldResult: {$valueResult"}}
])*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id:"$supplier_id", total: {$sum: "$stock"}}}
]);

// Field Projections with Aggregation
/*
$project
- can be used when aggregating data to include/excludefields from the returned result
Syntax:
	{$project: {field: 1/0}}
*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id:"$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}}
]);

// sort
/*
$sort
- used to change the order of aggregated results
- providing -1 as the value will result in MongoDB sorting the documents in reversed order
Syntax:
	{$sort: {field: 1/-1}}
*/
db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id:"$supplier_id", total: {$sum: "$stock"}}},
		{$sort: {total: -1}}
]);


db.fruits.aggregate([
		{$group: {_id:"$origin", kinds: {$sum: 1}}}
]);

// gusto mong igroup by country: ideconstruct mo muna yung array - $unwind
// after ng $unwind, saka pa lang pwedeng igroup per country. so the code below will show 5 objects (china, US, Philippines, Ecuador, India) tapos yung number of kinds of fruits, lilitaw na per country

db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group: {_id:"$origin", kinds: {$sum: 1}}}
]);

// Section - SChema Design or Data Modelling
/*
- schema design/data modelling is an important feature when creating databases
- MongoDB documents can be categorized into normalized or denormalized/embedded data
- Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces oof info
- De-normalized data/embedded data design refers to a data structure where related pieces of info are added to a document as an embedded object
*/


// Normalized data schema design in one-to-one relationship:
var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "09123456789"
});

// para ilink yung owner_id sa supplier id:
db.suppliers.insertOne({
        name: "Juicy Fruits",
        contact: "09123456789",
        owner_id: ObjectId("62d54831f6bceccc5c8b7855")
});

// de-normalized data schema design with one-to-few relationship
db.suppliers.insert({
	name: "DEF Fruits",
	contact: "09123456789",
	address: [
		{street:"123 San Jose St.", city: "Manila"},
		{street: "367 Gil Puyat", city: "Makati"}
	]
});

// Both data structures are common practices but each of them has its own pros and cons

// Difference between Normalized and De-normalized
/*
Normalized
- it makes it easier to read info because separate documents can be retrieved.
- too straightforward
- in terms of querying results, it performs slower compared to ebedded data due to having to retrieve multiple documents at the same time
- this approach is useful for data structures where pieces of info are commonly operated on/changed
*/

/*
De-Normalized
-it makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve the documents
- if the data structure becomes too complex and long, it makes it more difficult to manipulate and access info
- This approach is applicable for data structures where pieces of info are commonly retrieved and rarely operated on/changed (sample user info) 

*/
// Mini-activity
/*
Create 2 variable with ObjectId() values:supplier and branch

insert a document with the following properties:
_id: supplier,
name: string,
contact: string,
branches: [
	branch
]


create a branch collection and insert an object with the following properties 
_id: similar as the "branch" field in the suppliers collection
name: string,
address: string,
city: string
supplier_id: the same id as the supplier in supplier collection
*/
var supplier = ObjectId();
var branch = ObjectId();

db.suppliers.insert({
	_id: supplier,
	name: "Mang Tomas",
	contact: "09999999999"
	branches: [
		branch: "Malolos"
	]
});